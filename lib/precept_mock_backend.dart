library precept_mock_backend;

import 'package:flutter/foundation.dart';
import 'package:precept_client/backend/backend.dart';
import 'package:precept_client/backend/data.dart';
import 'package:precept_client/backend/query.dart';
import 'package:precept_client/backend/response.dart';
import 'package:precept_client/precept/script/backend.dart';
import 'package:precept_client/precept/script/document.dart';

class MockBackendDelegate implements BackendDelegate {
  final PBackend config;

  Map<String, Map<String, dynamic>> get store => _backend.get(config);
  Duration delay = Duration(seconds: 2);


   MockBackendDelegate({@required this.config});

  @override
  Future<CloudResponse> delete({@required List<DocumentId> documentIds}) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<CloudResponse> executeFunction(
      {@required String functionName, Map<String, String> params}) {
    // TODO: implement executeFunction
    throw UnimplementedError();
  }

  @override
  Future<bool> exists({@required DocumentId documentId}) {
    return Future.delayed(
      delay,
          () => store.containsKey(documentId.toKey),
    );
  }

  @override
  Future<Data> fetch({@required String functionName, DocumentId documentId}) {
    // TODO: implement fetch
    throw UnimplementedError();
  }

  @override
  Future<Data> fetchDistinct({@required String functionName, Map<String, dynamic> params}) {
    // TODO: implement fetchDistinct
    throw UnimplementedError();
  }

  @override
  Future<List<Data>> fetchList({@required String functionName, Map<String, String> params}) {
    // TODO: implement fetchList
    throw UnimplementedError();
  }

  /// See [Backend.get]
  @override
  // Future<Data> get({@required DocumentId documentId}) {
  //   final entry = _store[documentId.toKey];
  //   return Future.delayed(
  //       delay,
  //       () => {
  //             if (entry == null)
  //               {throw APIException(message: "Document Id ${documentId.toKey} not found")}
  //             else
  //               return Data(data: entry)
  //           });
  // }

  @override
  Future<Data> get({@required DocumentId documentId}) {
    final entry = store[documentId.toKey];
    return Future.delayed(
        delay,
            () =>
        (entry == null)
            ? throw APIException(message: "Document Id ${documentId.toKey} not found")
            : Data(data: entry));
  }

  @override
  Future<Data> getDistinct({@required Query query}) {
    // TODO: implement getDistinct
    throw UnimplementedError();
  }

  @override
  Future<Stream<Data>> getDistinctStream({@required Query query}) {
    // TODO: implement getDistinctStream
    throw UnimplementedError();
  }

  @override
  Future<Data> getList({@required Query query}) {
    // TODO: implement getList
    throw UnimplementedError();
  }

  @override
  Future<Stream<List<Data>>> getListStream({@required Query query}) {
    // TODO: implement getListStream
    throw UnimplementedError();
  }

  @override
  Future<Stream<Data>> getStream({@required DocumentId documentId}) {
    // TODO: implement getStream
    throw UnimplementedError();
  }

  @override
  Future<CloudResponse> loadPreceptModel({@required int minimumVersion}) {
    // TODO: implement loadPreceptModel
    throw UnimplementedError();
  }

  @override
  Future<CloudResponse> loadPreceptSchema({@required int minimumVersion}) {
    // TODO: implement loadPreceptSchema
    throw UnimplementedError();
  }

  @override
  Future<CloudResponse> save({DocumentId documentId, @required Map<String, dynamic> data}) {
    // TODO: implement save
    throw UnimplementedError();
  }

  setData({@required Map<DocumentId, Map<String, dynamic>> data}) {
    store.clear();
    for (var entry in data.entries) {
      store[entry.key.toKey] = entry.value;
    }
  }
}


/// An incredibly simplistic backend for testing.
///
/// Allows for multiple instances, with 'connection strings'
/// Used as a singleton.
class MockBackend{
  final Map<String, Map<String, Map<String, dynamic>>> _instances = Map();

  createNew({@required String connectionString}){
    assert(connectionString !=null);
    final instance=Map<String, Map<String, dynamic>>();
    _instances[connectionString]=instance;
  }

  get(PBackend config) {
    Map<String, Map<String, dynamic>> store = this._instances[config.connection["key"]];
    if (store==null){
      throw MockBackendException("No store exists for key ${config.connection['key']}");
    }
    return store;
  }
}

final _backend=MockBackend();


class MockBackendException implements Exception {
  final String msg;

  const MockBackendException(this.msg);

  String errMsg() => msg;
}